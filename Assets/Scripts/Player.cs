﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Camera playerCamera;
    public float mouseSensitivity;
    public Vector2 camClampY;

    public float movementSpeed;

    Rigidbody rigid;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = true;

        rigid = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        //Mouse input for rotating the player and camera.
        {
            Vector3 middleOfScreen = new Vector3(Screen.width / 2, Screen.height / 2, 0);
            float camMovementX = Input.GetAxis("Mouse X") * mouseSensitivity;
            float camMovementY = -Input.GetAxis("Mouse Y") * mouseSensitivity;

            transform.eulerAngles = new Vector2(transform.eulerAngles.x, (transform.eulerAngles.y + camMovementX) % 360);

            float currentCamAngle = playerCamera.transform.localEulerAngles.x;
            if (currentCamAngle > 180)
                currentCamAngle -= 360;

            float newCamAngle = Mathf.Clamp(currentCamAngle + camMovementY, camClampY.x, camClampY.y);
            if (newCamAngle < 0)
                newCamAngle += 360;

            playerCamera.transform.localEulerAngles = new Vector2(newCamAngle % 360, playerCamera.transform.localEulerAngles.y);
        }

        //Keyboard input for moving the player.
        {
            float inputVert = Input.GetAxis("Vertical") * movementSpeed;
            float inputHor = Input.GetAxis("Horizontal") * movementSpeed;

            float rotationRad = transform.eulerAngles.y * (Mathf.PI / 180);

            var verticalAxisMovement = new Vector2(Mathf.Sin(rotationRad) * inputVert, Mathf.Cos(rotationRad) * inputVert);
            var horizontalAxisMovement = new Vector2(Mathf.Cos(rotationRad) * inputHor, -Mathf.Sin(rotationRad) * inputHor);
            var totalMovement = verticalAxisMovement + horizontalAxisMovement;

            rigid.velocity = new Vector3(totalMovement.x, 0, totalMovement.y);
            
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        rigid.angularVelocity = Vector3.zero;
    }
}
